#include <grpcpp/grpcpp.h>
#include <string>
#include <thread>
#include <mutex>
#include <vector>
#include <optional>
#include <chrono>
#include <iomanip>
#include <unistd.h>
#include <algorithm>
#include "twr.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;

using twr::twrRequest;
using twr::twrResponse;
using twr::Twr;

using namespace std;

enum Direction { LEFT = 0, RIGHT = 1 };

mutex mlog;
vector<int> topology;

vector<int> readInput(string input)
{
  std::vector<int> vect;

  std::stringstream ss(input);

  for (int i; ss >> i;) {
    vect.push_back(i);
    if (ss.peek() == ',') ss.ignore();
  }

  return vect;
}

int getNodeByDistance(int myOrder, int distance, Direction direction)
{
  int node = -1;
  if (direction == LEFT) {
    node = myOrder + distance;
  } else {
    node = myOrder - distance;
  }

  if (node <= 0) {
    node += topology.size();
  } else if (node > topology.size()) {
    node -= topology.size();
  }

  return topology.at(node - 1);
}

void print(const string &s) {
  mlog.lock();
  cout << s << endl;
  mlog.unlock();
}


class twrNode : public Twr::Service
{
private:
  mutex mSend;
  // stubs
  string neighborAddressL = "";
  string neighborAddressR = "";
  shared_ptr<Twr::Stub> stubL;
  shared_ptr<Twr::Stub> stubR;
  shared_ptr<Channel> channelL;
  shared_ptr<Channel> channelR;
  // skeletons
  string skeletonAddressL = "";
  string skeletonAddressR = "";
  thread *tSkeletonL = NULL;
  thread *tSkeletonR = NULL;
  shared_ptr<Server> skeletonL;
  shared_ptr<Server> skeletonR;
  bool readyL = false;
  bool readyR = false;

  int myOrder = -1;
  int nodeId = -1;
  int leaderId = -1;
  bool defeated = false;
  int phase = 0;
  int received_election = -1;
  vector<int> recievedFromLeft;
  vector<int> recievedFromRight;

  void standDefeated() {
    if (this->defeated == false) {
      print("I AM DEFEATED: " + to_string(this->nodeId));
      this->defeated = true;
      recievedFromLeft.clear();
      recievedFromRight.clear();
    }
  }

  void log(const twr::twrRequest &request, const string& additional = "") const {
    const std::chrono::time_point<std::chrono::system_clock> now =
        std::chrono::system_clock::now();
    const std::time_t t_c = std::chrono::system_clock::to_time_t(now);

    mlog.lock();
    cout << "<" << this->nodeId << ", "
         << std::put_time(std::localtime(&t_c), "%T") << ", "
         << request.senderid() << ", " << request.receiverid()
         << ">: " << request.content() << " " << additional << endl;
    mlog.unlock();
  }

public:

  int getLeaderId() const {
    return this->leaderId;
  }

  twrNode(int myOrder, int nodeId, string skeletonAddressL,
          string skeletonAddressR, string neighborAddressL,
          string neighborAddressR) {
    this->myOrder = myOrder;
    this->nodeId = nodeId;
    this->skeletonAddressL = skeletonAddressL;
    this->skeletonAddressR = skeletonAddressR;
    this->neighborAddressL = neighborAddressL;
    this->neighborAddressR = neighborAddressR;
  }

  // send messag to neighbor stub
  void sendMessage(int receiverId, int senderId, string content, Direction direction)
  {
    // LOCK
    this->mSend.lock();
    // assemble request
    twrRequest request;
    request.set_receiverid(receiverId);
    request.set_senderid(senderId);
    request.set_content(content);
    request.set_direction(direction);
    // this->log(request, "sending");
    cout << "SENDING" << endl;
    // container for server response
    twrResponse reply;
    // Context can be used to send meta data to server or modify RPC behaviour
    ClientContext context;
    // Actual Remote Procedure Call
    optional<Status> status;
  

    if (direction == LEFT) {
      status = stubL->sendMessage(&context, request, &reply);
    } else {
      status = stubR->sendMessage(&context, request, &reply);
    }

    // Returns results based on RPC status
    if (status->ok()) {
    } else {
      cout << status->error_code() << ": " << status->error_message() << endl;
    }

    // UNLOCK
    this->mSend.unlock();
  }

  void transferMessage(const twrRequest &request) {
    Direction direction = request.direction() == LEFT ? LEFT : RIGHT;
    sendMessage(request.receiverid(),request.senderid(),request.content(), direction);
  }

  void sendMessage(int receiverId, const string &content, Direction direction) {
    sendMessage(receiverId, this->nodeId, content, direction);
  }

  void replyMessage(const twrRequest &request, const string& content) {
    const Direction reqDir = request.direction() == LEFT ? LEFT : RIGHT;
    // Send to left -> i received from right -> reply to right
    const Direction replyDir = reqDir == LEFT ? RIGHT : LEFT;
    this->sendMessage(request.senderid(), content, replyDir);
  }

  void sendEM(int receiverid, Direction direction) {
    if (this->defeated)
      return;
    
    this->sendMessage(receiverid, "EM", direction);
  }

  void processYes(const twrRequest &request) {
    if (this->defeated)
      return;

    this->received_election++;
    // print("RECEIVED " + to_string(this->received_election) + " NODEID: " + to_string(this->nodeId));
    // Send to right -> i received from left
    Direction receivedFrom = request.direction() == RIGHT ? LEFT : RIGHT;


    if (receivedFrom == LEFT) {
      this->recievedFromLeft.emplace_back(request.senderid());
    } else {
      this->recievedFromRight.emplace_back(request.senderid());
    }

    if (this->received_election == 2) {
      // Are there overlaps?
      vector<int> intersection;
      sort(this->recievedFromLeft.begin(), this->recievedFromLeft.end());
      sort(this->recievedFromRight.begin(), this->recievedFromRight.end());
      set_intersection(
          this->recievedFromLeft.begin(), this->recievedFromLeft.end(),
          this->recievedFromRight.begin(), this->recievedFromRight.end(),
          back_inserter(intersection));
      // Yes, there are overlaps, i'm a leader
      if (intersection.size() != 0) {
        this->leaderId = this->nodeId;
        this->sendMessage(this->nodeId, "LEADER", LEFT);
      }
      // No, there are no overlaps, i'm not a leader yet
      else {
        usleep(200000);
        this->findLeader();
      }
    }
  }

  void processMessage(const twrRequest &request) {

    if (request.content() == "EM") {
      if (request.senderid() < this->nodeId) {
        this->replyMessage(request, "YES");
      }
    }

    if (request.content() == "YES") {
      this->processYes(request);
    }

    if (request.content() == "LEADER") {
      
    }
  }

  // RECEIVE MESSAGE
  Status sendMessage(ServerContext* context, const twrRequest* request, twrResponse* reply) override
  {
    this->log(*request, "received");

    if (request->senderid() < this->nodeId) {
      this->standDefeated();
    }

    // message is for this node
    if (request->receiverid() == nodeId) {
      processMessage(*request);
    } else {
      if (request->content() == "LEADER")
        this->leaderId = request->senderid();

      transferMessage(*request);
    }
    // message processed
    return Status::OK;
  }

  void findLeader() {
    if (this->defeated)
      return;

    this->received_election = 0;
    this->phase += 1;
    print("STARTING: " + to_string(this->nodeId) + " PHASE: " + to_string(this->phase));
    // send message to left neighbor
    this->sendEM(getNodeByDistance(myOrder, this->phase, LEFT), LEFT);
    // print("SENT LEFT: " + to_string(this->nodeId));
    // send message to right neighbor
    this->sendEM(getNodeByDistance(myOrder, this->phase, RIGHT), RIGHT);
    // print("SENT RIGHT: " + to_string(this->nodeId));
  }

  void injectMessage(int receiverId, string content, Direction direction)
  {
    sendMessage(receiverId, this->nodeId, content, direction);
  }

  void createSkeleton(Direction direction)
  {
    shared_ptr<Server> skeleton = direction == LEFT ? this->skeletonL : this->skeletonR;
    string server_address(direction == LEFT ? this->skeletonAddressL : this->skeletonAddressR);
    ServerBuilder builder;
    // Listen on the given address without any authentication mechanism
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    // Register "service" as the instance through which
    // communication with client takes place
    builder.RegisterService(this);
    // Assembling the skeleton interface
    skeleton = builder.BuildAndStart();
    // server started;
    if (direction == LEFT) {
      this->readyL = true;
    } else {
      this->readyR = true;
    }

    skeleton->Wait();
  }

  // creating a single thread serving as skeleton
  void startSkeletons()
  {
    this->tSkeletonL = new thread(&twrNode::createSkeleton, this, LEFT);
    this->tSkeletonR = new thread(&twrNode::createSkeleton, this, RIGHT);
    // wait until the thread is ready
    while (this->readyL != true || this->readyR != true) {}
  }

  void startStubs()
  {
    this->channelL = grpc::CreateChannel(this->neighborAddressL, grpc::InsecureChannelCredentials());
    this->stubL = Twr::NewStub(this->channelL);
    this->channelR =  grpc::CreateChannel(this->neighborAddressR, grpc::InsecureChannelCredentials());
    this->stubR = Twr::NewStub(this->channelR);
  }
};

int main(int argc, char* argv[])
{
  if (argc != 2) {
    cerr << "Invalid number of arguments, should be 1, argc = " << argc << endl;
    return 1;
  }

  topology = readInput(argv[1]);
  int correctLeaderId = *(min_element(topology.begin(), topology.end()));
  vector<shared_ptr<twrNode>> ring;
  int nodes = topology.size();
  vector<thread> pool;

  // creating nodes
  for (int order = 1; order <= nodes; ++order) {
    int nodeR = order - 1 <= 0 ? nodes : order - 1;
    int nodeL = order + 1 > nodes ? 1 : order + 1;
    string skeletonAddressL = "127.0.0.1:6000" + std::to_string(order);
    string skeletonAddressR = "127.0.0.1:6100" + std::to_string(order);
    string neighborAddressL = "127.0.0.1:6100" + std::to_string(nodeL);
    string neighborAddressR = "127.0.0.1:6000" + std::to_string(nodeR);
    ring.emplace_back(make_shared<twrNode>(order, topology.at(order - 1),
                                           skeletonAddressL, skeletonAddressR,
                                           neighborAddressL, neighborAddressR));
  }

  // starting skeletons
  for (auto node : ring) node->startSkeletons();

  // starting stubs
  for (auto node : ring) node->startStubs();

  // sending one message over the entire ring
  // ring[0]->injectMessage(correctLeaderId, "", LEFT);
  // ring[0]->injectMessage(correctLeaderId, "", RIGHT);

  for (auto node : ring) {
    pool.emplace_back([&]{
      node->findLeader();
    });
  }

  for (auto node : ring) {
    while (node->getLeaderId() == -1) {
      usleep(500000);
    }
    print("LEADER: " + to_string(node->getLeaderId()));
  }

  cout << "Correct leader id: " << correctLeaderId << endl;

  for (auto &t : pool) {
    t.join();
  }

  exit(0);
}


  // void processYes(const twrRequest &request) {
  //   if (this->defeated)
  //     return;

  //   // Send to right -> i received from left
  //   Direction receivedFrom = request.direction() == RIGHT ? LEFT : RIGHT;

    
  //   this->received_election++;
  //   print("RECEIVED " + to_string(this->received_election) + " NODEID: " + to_string(this->nodeId));
  //   if (receivedFrom == RIGHT) {
  //     // Is there overlaps?
  //     auto it = find(recievedFromLeft.begin(), recievedFromLeft.end(), request.senderid());
  //     if (it == recievedFromLeft.end()) {
  //       // No -> i'm not a leader now
  //       if (received_election == 2)
  //         this->findLeader();

  //     } else {
  //       // Yes -> i'm leader
  //       this->leaderId = this->nodeId;
  //       this->sendMessage(this->nodeId, "LEADER", LEFT);
  //     }
  //   } else {
  //     this->recievedFromLeft.emplace_back(request.senderid());
  //   }
  // }